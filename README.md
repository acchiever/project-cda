# Project CDA

## Description
In this project Click DataStream of an e-commerce website is analysed in order to acquire
insights on the company's performance and current operational behaviour which in return 
would allow the members to make some important decisions in order to contribute toward 
the organization's growth.

## Visuals
![click_datastream.png](https://gitlab.com/acchiever/project-cda/-/blob/main/click_datastream.png)

## Installation
Follow installations of all the tools using instructions provided on their proprietary site.

## Authors and acknowledgment
Shubham Kakade

## License
Open Source

## Project status
Completed development
