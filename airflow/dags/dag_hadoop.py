#!/usr/bin/env python
# coding: utf-8

import airflow
import pendulum
from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.dummy import DummyOperator
import os
from airflow.utils.trigger_rule import TriggerRule


default_args = {
    'owner': 'hadoop',
    'start_date': pendulum.today('Asia/Kolkata').add(days=0),
    # 'end_date': datetime(),
    # 'depends_on_past': False,
    # 'email': ['airflow@example.com'],
    # 'email_on_failure': False,
    #'email_on_retry': False,
    # If a task fails, retry it once after waiting
    # at least 5 minutes
    #'retries': 1,
}

dag_execute_hdfs_commands = DAG(
    dag_id='hdfs_commands',
    default_args=default_args,
    # schedule_interval='0 0 * * *',
    schedule_interval='@once',
    description='executing hdfs commands',
)

# Hadoop FS job DAG 
create_dir = BashOperator(
                task_id="create_dir",
                bash_command="hdfs dfs -mkdir -p /user/hadoop/click  ",
                dag=dag_execute_hdfs_commands,
                run_as_user='hadoop'
                )


path = "/home/hadoop/click_data"
files = os.listdir(path)
if len(files)>0:
    filename = files[0]
else:
    filename = ''


file_check = BashOperator(
            task_id="file_check",
            bash_command="hdfs dfs -ls /user/hadoop/click/"+filename+" ",
            dag=dag_execute_hdfs_commands,
            run_as_user='hadoop'
            )


copy_from_local = BashOperator(
            task_id="copy_from_local",
            bash_command="hdfs dfs -put /home/hadoop/click_data/"+filename+" /user/hadoop/click  ",
          dag=dag_execute_hdfs_commands,
          run_as_user='hadoop',
	  trigger_rule=TriggerRule.ONE_SUCCESS
            )

# Job executing order
create_dir>>file_check
create_dir>>copy_from_local
if __name__ == '__main__ ':
    dag_execute_hdfs_commands.cli()

