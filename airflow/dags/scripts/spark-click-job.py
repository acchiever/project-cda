#!/usr/bin/env python
# coding: utf-8

# Imports
from pyspark.sql import SparkSession,functions as f

# Initializing Spark Session
spark = SparkSession\
        .builder\
        .appName("click-job")\
        .master("yarn")\        # Master as YARN
        .enableHiveSupport()\   # Hive support enabled, by default localhost:10000 
        .getOrCreate()
      
# selecting the correct database
spark.sql("use clickdb")

# JOB 1
#month_wise_user_visits
df = spark.sql("select month,count(month) as visits from click_data group by month order by visits desc")
lookup = {"01":"jan","02":"feb","03":"mar","04":"april","05":"may","06":"june","07":"july","08":"aug","09":"sep","10":"oct","11":"nov","12":"dec"}
df_final = df.replace(to_replace=lookup,subset=['month'])

#JOB 2
#Most_popular_products
testdf = spark.sql("select url,action from click_data")
testdf_filtered = testdf.withColumn('url',f.split(testdf.url,"\?")).select(f.element_at(f.col('url'),-1).alias('product'),f.col('action'))
testdf_grouped = testdf_filtered.groupBy("product").pivot("action").count().sort(f.col("purchase").desc())

#JOB 3
#Most_popular_categories
testdf_cat = testdf.withColumn('url',f.split(testdf.url,"\?")).select(f.element_at(f.col('url'),-2).alias("category"),f.col('action'))
testdf_cat = testdf_cat.withColumn('category',f.split(testdf_cat.category,"/")).select(f.element_at(f.col('category'),-1).alias("category"),f.col('action'))
testdf_cat_grouped = testdf_cat.groupBy("category").pivot("action").count().sort(f.col("purchase").desc())

#JOB 1 write
df.write.mode("append").saveAsTable("month_wise_user_visits")    # will create a new internal table in HIVE at userdb.da 
# JOB 2 write
testdf_grouped.write.mode("append").saveAsTable("most_popular_products")       # will create a new internal table in HIVE at userdb.db 
# JOB 2 write
testdf_cat_grouped.write.mode("append").saveAsTable("most_popular_categories")       # will create a new internal table in HIVE at userdb.db 

spark.stop() # Terminating spark session
