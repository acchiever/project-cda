#!/usr/bin/env python
# coding: utf-8

# In[3]:


from faker import Faker
import random
from randomtimestamp import randomtimestamp, random_date, random_time
import re 
from datetime import datetime
import os
import json
import shutil


# In[4]:

def datagen():
    path = "/home/hadoop/click_data/"
    filename = 'click*.json'
    fake = Faker()


    # In[5]:

    def gen_url():
        return "https://www.shop.com/"+fake.uri_page()+"/"+fake.uri_path()
    #gen_url_udf = f.udf(gen_url) 


    # In[6]:


    def gen_userID():
        return fake.user_name()
    #gen_userID_udf = f.udf(gen_userID)


    # In[7]:


    def action():
        actions = ['view','addToCart','removeFromCart','purchase']
        return random.choice(actions)
    #action_udf = f.udf(action)


    # In[8]:


    def log_time():
        return randomtimestamp(start_year=2020, end_year=2022, text=True, pattern="%Y-%m-%d %H:%M:%S")
    #log_time_udf = f.udf(log_time)


    # In[9]:


    def payment_method():
        pay_methods = ['Credit Card','COD','UPI','Debit Card','EMI','Wallet']
        return random.choice(pay_methods)
    #payment_method_udf = f.udf(payment_method)


    # In[10]:


    def gen_date(str):
        return re.search('^\d{4}-\d+-\d+',str).group(0)
    #gen_date_udf = f.udf(gen_date)


    # In[11]:


    def sessionID():
        return "Session_clickShop"+str(fake.random_number(digits=14))
    #sessionID_udf = f.udf(sessionID)


    # In[12]:


    def location():
        return fake.city()
    #location_udf = f.udf(location)


    # In[ ]:


    #columns = ['UserID','url','action','log_time','payment_method','location','SessionID','log_date']


    # In[ ]:


    # log_time()


    # In[13]:


    data_list = []
    for x in range(200000):
        log_t = log_time()
        log_d = gen_date(log_t)
        data_list.append({'userid':gen_userID(),'url':gen_url(),'action':action(),'log_time':log_t
                          ,'payment_method':payment_method(),'location':location(),'sessionID':sessionID(),'log_date':log_d})


    # In[19]:

    now = datetime.now()
    dt_string = now.strftime("%d-%m-%Y_%Hh%Mm%Ss")


    # In[20]:

    if not os.path.exists(path):
        os.makedirs(path)

    if os.path.exists(path + file_name):
        os.makedirs(path+'/old')
        shutil.move(src_folder + file_name, path+'/old')
        
    with open(path+'click_datagen_'+dt_string+'.json', 'w') as fp:
        for x in data_list:
            json.dump(x, fp)
            fp.write("\n")
    

