#!/usr/bin/env python
# coding: utf-8

# Import Libraries
from faker import Faker
import random
from randomtimestamp import randomtimestamp, random_date, random_time
import re
from datetime import datetime
import os
import json
import shutil
#import fnmatch

# Dummy ref lists 
category_list = ['Toy']*10+['Games']*40+['Sports']*30+['Consoles']*40
Toy = ['Toy train']*10+['Hot wheels']*10+['Bey Blade']*5+['RC car']*25+['RC plane']*30+['action figure']*35
Games = ['GTA V']*40+['MAFIA4']*10+['Witcher 3']*25+['Forza Horizon']*30+['Call of Duty AW']*5+['MARIO']*20
Consoles = ['PlayStation 3']*10+['PlayStation 4']*20+['PlayStation 5']*30+['XBOX One']*20+['XBOX one S']*10+['XBOX one X']*40
Sports = ['English Willow BAT']*30+['KOOKABURA BALL']*40+['Table Tennis BAT']*20+['Table Tenis BALL']*30+['FootBall']*50+['BasketBall']*5

# Datagen function 
def datagen():
    '''USed for generating dummy/fake data in json format'''
    path = "/home/hadoop/click_data"
    file_name = 'click*.json'
    fake = Faker()

    def gen_url():
        cat = random.choice(category_list)
        if cat == 'Toy':
            item = random.choice(Toy)
        elif cat == 'Games':
            item = random.choice(Games)
        elif cat == 'Consoles':
            item = random.choice(Consoles)
        elif cat == 'Sports':
            item = random.choice(Sports)
        else:
            item = 'xxx'
        return "https://www.shop.com/"+"Category"+"/"+cat+"?"+item

    def gen_userID():
        ''' Generates fake IDS, unique for users '''
        return fake.user_name()

    def action():
        ''' Generates actions [provided pre specified set of actions] '''
        actions = ['view']*70+['addToCart']*50+['removeFromCart']*40+['purchase']*40
        return random.choice(actions)

    def log_time():
        ''' Generates timestamp in YYYY-MM-DD HH:MM:SS format '''
        return randomtimestamp(start_year=2020, end_year=2022, text=True, pattern="%Y-%m-%d %H:%M:%S")

    def payment_method():
        ''' Generates payment_method [provided pre specified set of payment_methods] '''
        pay_methods = ['Credit Card']*30+['COD']*10+['UPI']*40+['Debit Card']*40+['EMI']*15+['Wallet']*20
        return random.choice(pay_methods)

    def gen_date(str):
        ''' Generates date by taking a subset of previously generated timestamp, format YYYY-MM-DD '''
        return re.search('^\d{4}-\d+-\d+',str).group(0)

    def sessionID():
        ''' Generates session_ID, unique for all session ever created '''
        return "Session_clickShop"+str(fake.random_number(digits=14))

    def location():
        ''' Generates fake location [city] '''
        return fake.city()

    # creating rows 
    data_list = []
    for x in range(200000):   # 200000 represents the number of rows to be generated
        log_t = log_time()
        log_d = gen_date(log_t)
        data_list.append({'userid':gen_userID(),'url':gen_url(),'action':action(),'log_time':log_t
                          ,'payment_method':payment_method(),'location':location(),'sessionID':sessionID(),'log_date':log_d})

    # Provides current timestamp 
    now = datetime.now()
    dt_string = now.strftime("%d-%m-%Y_%Hh%Mm%Ss")

    # Paths
    if not os.path.exists(path):
        os.makedirs(path)

    if not os.path.exists(path + '/old'):
        os.makedirs(path+'/old')
    files = os.listdir(path)

    name = files[0]
    shutil.move(path +'/'+ name, path+'/old')   # moving previously processed files to old directory

    with open(path+'/'+'click_datagen_'+dt_string+'.json', 'w') as fp:  #appending timestamp for representing creation of json file
        for x in data_list:
            json.dump(x, fp)
            fp.write("\n")
