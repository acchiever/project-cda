#!/usr/bin/env python
# coding: utf-8

# LIBRARIES
from airflow import DAG
import pendulum
from airflow.operators.python import PythonOperator
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from airflow.models import Variable
from airflow.providers.apache.hive.operators.hive import HiveOperator
from scripts import datagen
from airflow.operators.bash import BashOperator
from airflow.operators.dummy import DummyOperator
import os
from airflow.utils.trigger_rule import TriggerRule
import datetime



path = "/home/hadoop/click_data"
files = os.listdir(path)
if len(files)>0:
    filename = files[0]
else:
    filename = ''

# INITIALIZING DAG
work_flow_dag_id = "click_job"


work_flow_start_date = pendulum.today('Asia/Kolkata').add(days=0)


work_flow_email = ["acchiever@gmail.com"]


work_flow_default_args = {
    "owner":"hadoop",
    "email":work_flow_email,
    "email_on_failure":False
    }


dag = DAG(
    dag_id=work_flow_dag_id,
    schedule_interval=None,
    start_date=work_flow_start_date,
    default_args=work_flow_default_args
 )


################ DATAGEN DAG ######################
with dag:
    click_data = PythonOperator(
        task_id = 'click_data',
        python_callable = datagen.datagen,
    )


################### HADOOP job DAGS ########################
create_dir = BashOperator(
                task_id="create_dir",
                bash_command="hdfs dfs -mkdir -p /user/hadoop/click  ",
                dag=dag,
                run_as_user='hadoop'
                )

mr_jars=Variable.get("MR_JARS")
mr_job1 = BashOperator(
            task_id="mr_job1",
            bash_command=f"hadoop jar {mr_jars}Click-0.0.1-SNAPSHOT.jar Click_Driver file://"+path+"/"+filename+" /user/hadoop/click_output ",
            dag=dag,
            #run_as_user='hadoop'
            )
mr_job2 = BashOperator(
            task_id="mr_job2",
            bash_command=f"hadoop jar {mr_jars}Mr_pay-0.0.1-SNAPSHOT.jar MrPay_Driver file://"+path+"/"+filename+" /user/hadoop/click_output1 ",
            dag=dag,
            #run_as_user='hadoop'
            )

mr_job1_clr = BashOperator(
                task_id="click_mr_job1_clr",
                bash_command="hdfs dfs -rm -R /user/hadoop/click_output ",
                dag=dag,
                #run_as_user='hadoop'
               )

mr_job2_clr = BashOperator(
                task_id="click_mr_job2_clr",
                bash_command="hdfs dfs -rm -R /user/hadoop/click_output1 ",
                dag=dag,
                #run_as_user='hadoop'
               )


file_check = BashOperator(
            task_id="file_check",
            bash_command="hdfs dfs -ls /user/hadoop/click/"+filename+" ",
            dag=dag,
            run_as_user='hadoop'
            )


copy_from_local = BashOperator(
            task_id="copy_from_local",
            bash_command="hdfs dfs -put /home/hadoop/click_data/"+filename+" /user/hadoop/click  ",
          dag=dag,
          run_as_user='hadoop',
	  trigger_rule=TriggerRule.ONE_SUCCESS
            )


############### HIVE job DAGS #####################
execute_hql_script = HiveOperator(
    hql = "hql/table.hql",
    task_id = "executehql_task",
    hive_cli_conn_id = "hive_cli_default",
    dag = dag
)

mr_hive_load = HiveOperator(
    hql = "hql/mr_job.hql",
    task_id = "mr_op_table",
    hive_cli_conn_id = "hive_cli_default",
    dag = dag
)

hive_job = HiveOperator(
    hql = "hql/hive_job.hql",
    task_id = "hive_click_job",
    hive_cli_conn_id = "hive_cli_default",
    dag = dag
)

################# Spark job DAG #########################
pyspark_app_home=Variable.get("PYSPARK_APP_HOME")
with dag:
    spark_click_job= SparkSubmitOperator( 
    task_id='spark_click_job',
    conn_id='spark_default',
    application=f'{pyspark_app_home}/spark-click-job.py',
    #total_executor_cores=2,
    #packages="io.delta:delta-core_2.12:0.7.0,org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.0",
    #executor_cores=2,
    #executor_memory='1g',
    #driver_memory='1g',
    name='spark_click_job',
   )


######### The order in which tasks are been executed 
click_data>>create_dir>>file_check
create_dir>>copy_from_local>>execute_hql_script>>spark_click_job
execute_hql_script>>mr_job1>>mr_job2>>mr_hive_load>>mr_job1_clr>>mr_job2_clr
execute_hql_script>>hive_job
