#!/usr/bin/env python
# coding: utf-8

# In[1]:


from airflow import DAG
import pendulum
from airflow.operators.python import PythonOperator
from scripts import datagen
import datetime


# In[2]:


work_flow_dag_id = "click_data_gen"


# In[3]:


work_flow_start_date = pendulum.today('Asia/Kolkata').add(days=0)


# In[4]:


work_flow_schedule_interval = '@once'


# In[5]:


work_flow_email = ["acchiever@gmail.com"]


# In[6]:


work_flow_default_args = {
    "owner":"hadoop",
    "start_date":work_flow_start_date,
    "email":work_flow_email,
    "email_on_failure":False
    }


# In[7]:


dag_click_data_gen = DAG(
    dag_id=work_flow_dag_id,
    schedule_interval=work_flow_schedule_interval,
    default_args=work_flow_default_args)


# In[ ]:


# bash_task = BashOperator(
#     task_id='bash_task',
#     bash_command='python3 scripts/datagen.py ',
#     dag=dag)


# In[9]:


with dag_click_data_gen:
    click_data = PythonOperator(
        task_id = 'click_data',
        python_callable = datagen.datagen,
    )


# In[ ]:

if __name__ == "__main__":
    dag_click_data_gen.cli()


