#!/usr/bin/env python
# coding: utf-8

# In[3]:


import airflow
import pendulum
from airflow import DAG
from airflow.providers.apache.hive.operators.hive import HiveOperator
from airflow.utils.dates import days_ago


# In[6]:


default_args = {
    'owner': 'hadoop',
    'start_date': pendulum.today('Asia/Kolkata').add(days=0),
    # 'end_date': datetime(),
    # 'depends_on_past': False,
    # 'email': ['airflow@example.com'],
    # 'email_on_failure': False,
    #'email_on_retry': False,
    # If a task fails, retry it once after waiting
    # at least 5 minutes
    #'retries': 1,
}


# In[5]:


dag_exec_scripts = DAG(
    dag_id='hive_insert',
    default_args=default_args,
    # schedule_interval='0 0 * * *',
    schedule_interval='@once',
    description='executing the sql and hql scripts',
)


# In[ ]:


execute_hql_script = HiveOperator(
    hql = "hql/table.hql",
    task_id = "executehql_task",
    hive_cli_conn_id = "hive_cli_default",
    dag = dag_exec_scripts
)


# In[ ]:


if __name__ == "__main__":
    dag_exec_scripts.cli()

