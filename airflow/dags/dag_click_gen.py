#!/usr/bin/env python
# coding: utf-8


from airflow import DAG
import pendulum
from airflow.operators.python import PythonOperator
from scripts import datagen
import datetime


work_flow_dag_id = "click_data_gen"

work_flow_start_date = pendulum.today('Asia/Kolkata').add(days=0)

work_flow_schedule_interval = '@once'

work_flow_email = ["acchiever@gmail.com"]

work_flow_default_args = {
    "owner":"hadoop",
    "start_date":work_flow_start_date,
    "email":work_flow_email,
    "email_on_failure":False
    }
# Iitializing dag
dag_click_data_gen = DAG(
    dag_id=work_flow_dag_id,
    schedule_interval=work_flow_schedule_interval,
    default_args=work_flow_default_args)

# datagen DAG
with dag_click_data_gen:
    click_data = PythonOperator(
        task_id = 'click_data',
        python_callable = datagen.datagen,
    )


if __name__ == "__main__":
    dag_click_data_gen.cli()


