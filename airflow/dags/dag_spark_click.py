#!/usr/bin/env python
# coding: utf-8

# Imports
from airflow import DAG
import pendulum
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from  datetime import datetime, timedelta
from airflow.models import Variable

# DAG initialization Configs
work_flow_dag_id = "spark_click"
work_flow_start_date = pendulum.today('Asia/Kolkata').add(days=0)
work_flow_schedule_interval = '@once'
work_flow_email = ["acchiever@gmail.com"]
work_flow_default_args = {
    "owner":"hadoop",
    "start_date":work_flow_start_date,
    "email":work_flow_email,
    "email_on_failure":False
    )
# Initializing DAG    
dag = DAG(
    dag_id=work_flow_dag_id,
    schedule_interval=work_flow_schedule_interval,
    default_args=work_flow_default_args)

# Spark Job DAG initialization
pyspark_app_home=Variable.get("PYSPARK_APP_HOME") # Acquiring declared airflow variable
with dag:
    spark_click_job= SparkSubmitOperator( 
    task_id='users_with_most_purchases',
    conn_id='spark_default',
    application=f'{pyspark_app_home}/spark-click-job.py',
    total_executor_cores=2,
    #packages="io.delta:delta-core_2.12:0.7.0,org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.0",
    executor_cores=2,
    #executor_memory='1g',
    #driver_memory='1g',
    name='users_with_most_purchases',
    )

if __name__ == "__main__":
    spark_click_job.cli()
