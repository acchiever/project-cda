#switching database
use clickdb;

#mr_job1
#creating new table mr_action_count for storing mr_job1 output
create table if not exists mr_action_count(action string,count int) row format delimited fields terminated by '\t';

#loading data from mr_job1 output generated file into above created table
load data inpath "hdfs://localhost:9000/user/hadoop/click_output/part-r-00000" into table mr_action_count;

# mr_job2
#creating new table preferred_pay_method for storing mr_job2 output
create table if not exists preferred_pay_method(payment_method string,count int) row format delimited fields terminated by '\t';

#loading data from mr_job2 output generated file into above created table
load data inpath "hdfs://localhost:9000/user/hadoop/click_output1/part-r-00000" into table preferred_pay_method;
