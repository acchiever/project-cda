# switching db
use clickdb;

#HIVE job 1
#creating table location_wise_trafic
create table if not exists location_wise_trafic (city string, visitors int);

#executing query on click_data and loading result into location_wise_trafic table
insert into table location_wise_trafic select location,count(location) as num_visitors from click_data group by location;

#HIVE job 2
#creating table most_active_users
create table if not exists most_active_users(username string,visits int);

#executing query on click_data and loading result into most_active_users table
insert into table most_active_users select y.userid as username,y.visits as visited from (select userid,count(userid) as visits from click_data group by userid) y where y.visits in (select x.visits from (select userid,count(userid) as visits from click_data group by userid)x sort by x.visits desc limit 10) order by visited desc;

