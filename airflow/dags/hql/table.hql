# setting hive properties essential for dynamic partitioning
set hive.exec.dynamic.partition=true;
set hive.exec.dynamic.partition.mode=nonstrict;

#creating db if not found
create database if not exists clickdb;

#switching db
use clickdb;

#creating external table click in order read obtained json and parse it in tabular form 
create external table if not exists click(UserID string,url string,action string,log_time string,payment_method string,location string,sessionID string,log_date string) row format serde 'org.apache.hive.hcatalog.data.JsonSerDe' location 'hdfs://localhost:9000/user/hadoop/click';

#creating another table click_data which will have partitioned data, used for quering as well as archiving data
create table if not exists click_data(UserID string,url string,action string,log_time string,payment_method string,location string,sessionID string,log_date string) partitioned by (month string);

#load data into click_data with partitioning on month
insert overwrite table click_data partition(month) select userid,url,action,log_time,payment_method,location,sessionid,log_date,substring(log_date,6,2) from click;
