#!/usr/bin/env python
# coding: utf-8

#imports
from airflow.providers.apache.hive.operators.hive import HiveOperator
import pendulum
from airflow.models import Variable
from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.dummy import DummyOperator
import os
from airflow.utils.trigger_rule import TriggerRule
from datetime import datetime, timedelta


default_args = {
    'owner': 'hadoop',
    'start_date': pendulum.today('Asia/Kolkata').add(days=0),
    # 'end_date': datetime(),
    # 'depends_on_past': False,
    # 'email': ['airflow@example.com'],
    # 'email_on_failure': False,
    #'email_on_retry': False,
    # If a task fails, retry it once after waiting
    # at least 5 minutes
    #'retries': 1,
}

# Initializing DAG
dag = DAG(
    dag_id='mr_job',
    default_args=default_args,
    # schedule_interval='0 0 * * *',
    schedule_interval=None,
    description='executing hdfs commands',
    dagrun_timeout=timedelta(minutes=60)
)

# MR Job DAG
path = "/home/hadoop/click_data"
files = os.listdir(path)
if len(files)>0:
    filename = files[0]
else:
    filename = ''

mr_jars=Variable.get("MR_JARS")
mr_job1 = BashOperator(
            task_id="mr_job1",
            bash_command=f"hadoop jar {mr_jars}Click-0.0.1-SNAPSHOT.jar Click_Driver file://"+path+"/"+filename+" /user/hadoop/click_output ",
            dag=dag,
            #run_as_user='hadoop'
            )
mr_job2 = BashOperator(
            task_id="mr_job2",
            bash_command=f"hadoop jar {mr_jars}Mr_pay-0.0.1-SNAPSHOT.jar MrPay_Driver file://"+path+"/"+filename+" /user/hadoop/click_output1 ",
            dag=dag,
            #run_as_user='hadoop'
            )

mr_job1_clr = BashOperator(
                task_id="click_mr_job1_clr",
                bash_command="hdfs dfs -rm -R /user/hadoop/click_output ",
                dag=dag,
                #run_as_user='hadoop'
               )

mr_job2_clr = BashOperator(
                task_id="click_mr_job2_clr",
                bash_command="hdfs dfs -rm -R /user/hadoop/click_output1 ",
                dag=dag,
                #run_as_user='hadoop'
               )

mr_hive_load = HiveOperator(
    hql = "hql/mr_job.hql",
    task_id = "mr_op_table",
    hive_cli_conn_id = "hive_cli_default",
    dag = dag
)

# Task executing order
mr_job1>>mr_job2>>mr_hive_load>>mr_job1_clr>>mr_job2_clr

if __name__ == '__main__ ':
    dag.cli()

